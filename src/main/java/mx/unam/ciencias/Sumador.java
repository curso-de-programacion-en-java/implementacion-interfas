package mx.unam.ciencias;

public interface Sumador{

  public double realizaSuma(double a, double b);

  public double realizaResta(double a, double b);

}
