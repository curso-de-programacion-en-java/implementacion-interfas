package mx.unam.ciencias;

public class Corre implements Sumador{
	
	@Override public double realizaSuma(double a, double b){
		return a+b;
	}

    @Override public double realizaResta(double a, double b){
    	return a-b;
    }


    public void run(){
    	System.out.println("Este programa es un claro ejemplo de como implementar interfaces");
    	double var1 = 12;
    	double var2 = 15;
    	System.out.printf("Se tomaran los valores de a=%d y de b=%d\n",var1,var2);
    	double resultadoSuma = realizaSuma(var1,var2);
    	double resultadoResta = realizaResta(var1,var2);
    	System.out.printf("El resultado de la suma es: %d",resultadoSuma);
        System.out.printf("El resultado de la resta es: %d",resultadoResta);

    }
}